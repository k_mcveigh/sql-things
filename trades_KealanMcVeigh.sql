--1
select t.*
from position p join trade t on (p.closing_trade_ID=t.ID or p.opening_trade_ID =t.ID)
where trader_id =1 and t.stock='Hon'


--2
select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from position p join trade t on (p.closing_trade_ID=t.ID or p.opening_trade_ID =t.ID)
where p.trader_id =1 and t.stock='Hon';

-- 3
create view loss_and_prof as

select tra.last_name, tra.first_name, sum(t.price*t.size) as 'profit'

from trade t join position po on po.closing_trade_id = t.id or po.opening_trade_id = t.id 
			join trader tra on po.trader_id = tra.id

where po.closing_trade_id is not null and po.closing_trade_id != po.opening_trade_id
group by tra.first_name, tra.last_name;

select * from loss_and_prof