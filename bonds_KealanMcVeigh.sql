--1
select * from bond
where CUSIP = '28717RH95'

--2
select * from bond
order by maturity ASC

--3
select sum(quantity*price) from bond

--4
select sum(quantity*coupon/100) from bond
group by ID

--5
select * from bond
where rating like 'AA%'

--6
select avg(price), avg(coupon), rating from bond
group by rating

--7
select b.*, r.expected_yield, (coupon/price)
from bond b join rating r on b.rating =r.rating
where (b.coupon/b.price)< r.expected_yield